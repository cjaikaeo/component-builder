import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="compbuilder",
    version="0.3.0",
    author="Jittat Fakcharoenphol",
    author_email="jittat@gmail.com",
    description="Python-based DSL for constructing and testing digital circuit components with HDL-like interface",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jittat/component-builder",
    packages=setuptools.find_packages(),
    install_requires=[
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering",
    ],
)
